<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Objects;

class ObjectsTest extends TestCase
{

    public function testGetAllRecords()
    {
        $response = $this->get('/api/object/get_all_records');
        $response->assertStatus(200);
    }

    public function testGetRecord()
    {
        $object = Objects::find(1);
        $response = $this->get('/api/object/'.$object->id);

        $response->assertStatus(200);
    }

    public function testGetByTimestamp()
    {
        $object = Objects::find(1);
        $response = $this->get('/api/object/100?timestamp='.strtotime($object->created_at));

        $response->assertStatus(200);
    }

    public function testPostObject()
    {
        $response = $this->post('/api/object/', [
            'title' => 'Title Test',
            'description' => 'Description Test'
        ]);
        $response->assertStatus(201);
    }

    public function testPostObjectExists()
    {
        $object = Objects::find(1);
        $response = $this->post('/api/object/', [
            'id' => $object->id,
            'title' => 'Title Test',
            'description' => 'Description Test'
        ]);
        $response->assertStatus(201);
    }

    public function testPostObjectMissingTitle()
    {
        $response = $this->post('/api/object/', [
            'description' => 'Description Test'
        ]);
        $response->assertStatus(400);
    }
}
