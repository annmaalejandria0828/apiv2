<?php

namespace App\Http\Controllers;

use App\Models\Objects;
use Illuminate\Http\Request;
use Validator;

class ObjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objects = Objects::get();
        return response()->json($objects, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'description' => 'required|max:255'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
            return response()->json($validator->errors(), 400);
        }

        if (isset($request->id))
            $object = Objects::where('id', $request->id)->first();
        if (isset($object))
        {
            $object->update($request->all());
        }
        else {
            $object = Objects::create($request->all());
        }
        return response()->json($object, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (isset($_GET['timestamp']))
        {
            $objects = Objects::where('created_at', '<=', date('Y-m-d H:i:s', $_GET['timestamp']))->get();
            // echo strtotime($objects[0]->created_at).' '.$objects[0]->created_at.' <= '.$_GET['timestamp'].' '.date('Y-m-d H:i:s', $_GET['timestamp']).' || ';
            // 1626971970 1626971971 1626972647
        }
        else {
            $objects = Objects::where('id', $id)->get();
        }
        return response()->json($objects, 200);
    }
}
